﻿//    Copyright (C) 2018  Glenn
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Browser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var t = timer1;
            textBox2.ForeColor = SystemColors.GrayText;
            textBox2.Text = "Google Search";
            textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            textBox2.Enter += new System.EventHandler(this.textBox2_Enter);
            t.Interval = 1;
            t.Tick += new EventHandler(timer1_Tick);
            t.Enabled = true;
            webBrowser1.Navigate(@"https://www.google.com");
            webBrowser1.DocumentCompleted += webBrowser1_DocumentCompleted;
            webBrowser1.ProgressChanged += ProgressChanged;
            webBrowser1.Navigating += Navigating;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (webBrowser1.CanGoBack == true)
            {
                button1.Enabled = true;
            }
            else
            {
                if (webBrowser1.CanGoBack == false)
                {
                    button1.Enabled = false;
                }
            }
            if (webBrowser1.CanGoForward == true)
            {
                button2.Enabled = true;
            }
            else
            {
                if (webBrowser1.CanGoForward == false)
                {
                    button2.Enabled = false;
                }
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Text = "Web Browser - " + webBrowser1.DocumentTitle.ToString();
            textBox1.Text = e.Url.ToString();
        }

        private void ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            try
            {
                progressBar1.Maximum = (int)e.MaximumProgress;
                progressBar1.Value = (int)e.CurrentProgress;
                label1.Text = e.CurrentProgress.ToString() + "%";
            }
            catch
            {

            }
            
        }
          
        private void Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            textBox1.Text = e.Url.ToString();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                webBrowser1.Navigate(textBox1.Text);
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                webBrowser1.Navigate( @"https://www.google.com/search?q="+ textBox2.Text);
            }
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text.Length == 0)
            {
                textBox2.Text = "Google Search";
                textBox2.ForeColor = SystemColors.GrayText;
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "Google Search")
            {
                textBox2.Text = null;
                textBox2.ForeColor = SystemColors.WindowText;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (webBrowser1.CanGoBack == true)
            {
                webBrowser1.GoBack();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (webBrowser1.CanGoForward == true)
            {
                webBrowser1.GoForward();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            webBrowser1.Stop();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }
    }
}
